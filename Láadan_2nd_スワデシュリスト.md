# 第2世代 Láadan スワデシュ･リスト 207 (暫定)

|№|英語|日本語|ラーダン|
|-:|-|-|-|
|1|I|私|le|
|2|you|君 / あなた|ne|
|3|he|彼|be|
|4|we|私達|lezh (2-5) / len (6以上)|
|5|you|君ら|nezh (2-5) / nen (6以上)|
|6|they|彼ら|bezh (2-5) / ben (6以上)|
|7|this|これ|hi|
|8|that|あれ|hithed|
|9|here|ここ|nu|
|10|there|そこ|núu|
|11|who|誰|bebáaden|
|12|what|何|bebáath|
|13|where|何処|bebáasha|
|14|when|いつ|bebáaya|
|15|how|どのように|bebáanal|
|16|not|否|ra|
|17|all|全て|woho|
|18|many|多く|menedebe|
|19|some|少し|‐ye‐|
|20|few|少ない|nedebe|
|21|other|他|thotho ?|
|22|one|一|nede|
|23|two|二|shin|
|24|three|三|boó|
|25|four|四|bim|
|26|five|五|shan|
|27|big|大きい|rahíya|
|28|long|長い|rabohí|
|29|wide|広い|rahíyamesh|
|30|thick|厚い|?|
|31|heavy|重い|sho|
|32|small|小さい|híya|
|33|short|短い|bohí ?|
|34|narrow|狭い|híyamesh ?|
|35|thin|細い|?|
|36|woman|女|withizh|
|37|man|男|withid|
|38|human|人|with ?|
|39|child|子|háawith|
|40|wife|妻|?|
|41|husband|夫|?|
|42|mother|母|thulizh|
|43|father|父|thulid ?|
|44|animal|生き物|mid|
|45|fish|魚|thili|
|46|bird|鳥|babí|
|47|dog|犬|lanemid|
|48|louse|虱|?|
|49|snake|蛇|snake|
|50|worm|虫|batheóobamid (6本足) ? nibeóobamid (8本足) ? meóobamid (多足) ?|
|51|tree|木|yáanin|
|52|forest|森|olin|
|53|stick|枝|nedebosh|
|54|fruit|実 / 果物|yu|
|55|seed|種|thon|
|56|leaf|葉|mi|
|57|root|根|root|
|58|bark|木皮|boshoya|
|59|flower|花|mahina|
|60|grass|草|hesh|
|61|rope|縄|methid|
|62|skin|皮|oya|
|63|meat|肉|deheni|
|64|blood|血|luhili|
|65|bone|骨|thud|
|66|fat|脂|zhebom|
|67|egg|卵|máa|
|68|horn|角|onethud|
|69|tail|尻尾|hoyo|
|70|feather|羽|yulish / hosh|
|71|hair|髪|delith|
|72|head|頭|on|
|73|ear|耳|oyu|
|74|eye|目|oyi|
|75|nose|鼻|oyo|
|76|mouth|口|óoyo|
|77|tooth|歯|dash|
|78|tongue|舌|odith|
|79|fingernail|爪|bath|
|80|foot|足|óoma|
|81|leg|脚|óoba|
|82|knee|膝|óobashinenil ?|
|83|hand|手|oma|
|84|wing|翼|yuloma|
|85|belly|腹|hod|
|86|guts|腸|hodáath|
|87|neck|首|womedim|
|88|back|背|wan|
|89|breast|胸|thol|
|90|heart|心|óoya (心臓) / ashon (第三者への愛情)|
|91|liver|肝|web|
|92|to drink|飲む|rilin|
|93|to eat|食べる|yod|
|94|to bite|噛む|dashobe|
|95|to suck|吸う|?|
|96|to spit|吐(つ)く|?|
|97|to vomit|吐(は)く|?|
|98|to blow|吹く|olob|
|99|to breathe|息する|wíyul|
|100|to laugh|笑う|ada|
|101|to see|見る|il|
|102|to hear|聞く|ma|
|103|to know|知る|an|
|104|to think|思う|lith|
|105|to smell|嗅ぐ|shu|
|106|to fear|恐れる|héeya|
|107|to sleep|寝る|áana|
|108|to live|生きる|habelid|
|109|to die|死ぬ|shebasheb|
|110|to kill|殺す|nowidá ?|
|111|to fight|戦う|rashon|
|112|to hunt|狩る|mideyodewanenowidá ?|
|113|to hit|打つ|olob|
|114|to cut|切る|humesh|
|115|to split|割る|shinethen ?|
|116|to stab|刺す|ihehumesh?|
|117|to scratch|掻く|ibath|
|118|to dig|掘る|?|
|119|to swim|泳ぐ|ilisháad|
|120|to fly|飛ぶ|shumáad|
|121|to walk|歩く|óomasháad|
|122|to come|来る|sháad|
|123|to lie (as in a bed)|寝る / 横たわる|rúu|
|124|to sit|座る|wod|
|125|to stand|立つ|thib|
|126|to turn (自動詞)|回る|wetham|
|127|to fall|落ちる|háda|
|128|to give|与える / 上げる|ban|
|129|to hold|持つ|widom|
|130|to squeeze|絞る|thamehib|
|131|to rub|擦る|?|
|132|to wash|洗う|éthe (掃除する)|
|133|to wipe|拭く|ralili (乾かす)|
|134|to pull|引く|razhida ?|
|135|to push|押す|zhida ?|
|136|to throw|投げる|thoshesháad / thosháad ?|
|137|to tie|結ぶ|?|
|138|to sew|縫う|adal|
|139|to count|数える|lamith|
|140|to say|言う|di|
|141|to sing|歌う|lalom|
|142|to play|遊ぶ|elash|
|143|to float|浮く|shumesháad ?|
|144|to flow|流れる|ilisháad ?|
|145|to freeze|凍る|widomili ?|
|146|to swell|膨らむ|delinenáraheb ?|
|147|sun|日|rosh|
|148|moon|月|óol|
|149|star|星|ash|
|150|water|水|ili|
|151|rain|雨|lali|
|152|river|川|wili|
|153|lake|湖|ramela ?|
|154|sea|海|mela|
|155|salt|塩|máan|
|156|stone|石|ud|
|157|sand|砂|sheshi|
|158|dust|塵|híyasheshi ?|
|159|earth|土|doni|
|160|cloud|雲|boshum|
|161|fog|霧|úushili|
|162|sky|空|thosh|
|163|wind|風|yul|
|164|snow|雪|hish|
|165|ice|氷|widomili ?|
|166|smoke|煙|?|
|167|fire|火|óowa|
|168|ash|灰|líithihíyasheshi ?|
|169|to burn|燃える|óowa ?|
|170|road|道|weth|
|171|mountain|山|bo|
|172|red|赤|laya|
|173|green|緑|liyen|
|174|yellow|黄色|léli|
|175|white|白|líithi|
|176|black|黒|loyo|
|177|night|夜|náal|
|178|day|日|sháal|
|179|year|年|hathóoletham|
|180|warm|暖かい|owa|
|181|cold|寒い|rahowa|
|182|full|満ちた|ume|
|183|new|新しい|rabalin ?|
|184|old|古い|balin|
|185|good|良い|thal|
|186|bad|悪い|rathal|
|187|rotten|腐った|balinerahéthe ?|
|188|dirty|汚い|rahéthe ?|
|189|straight|真っ直ぐ|raboósh|
|190|round|丸い|tham ?|
|191|sharp (as a knife)|鋭い|huma|
|192|dull (as a knife)|鈍い|rahuma ?|
|193|smooth|滑らか|?|
|194|wet|濡れた|lili|
|195|dry|乾いた|ralili|
|196|correct|正しい|dóon|
|197|near|近い|thoma|
|198|far|遠い|thed|
|199|right|右|hiwetho|
|200|left|左|hiwetha|
|201|at|(場所)で|‐sha|
|202|in|(～の中)で|‐nil|
|203|with|と(共に)|‐dan (中立) / ‐den (喜びを伴う)|
|204|and|と|i|
|205|if|もし|bere…ébere (もし…ならば)|
|206|because|何故なら|‐wáan|
|207|name|名前|zha|
