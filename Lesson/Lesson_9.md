# レッスン 9 シャは大会でパネルを持っています

## 9-1. このレッスンで使う単語

|||
|-|-|
|alehala|芸術(作品を作る)|
|bama|怒っている(理由はあるが、誰かのせいにしてどうにかなるようなものではなく、どうにかなるようなものでもない。)|
|dihomedal|討論、パネル|
|eth|～について|
|Dóo|ええと…(考えるための間を取る)|
|néde|～が欲しい、～を望む、～を要求する|
|||
|íi|(～も)また、(～と)同じく、～過ぎる、余りにも～|

## 9-2. 例文

|||
|-|-|
|Bíi ril thi Sha dihomedaleth wa.|シャはパネルを持っています|
|Bíi aril eth dihomedal alehala wa.|討論は芸術についてでしょう|
|Bíi ril thi ra óowamid dihomedaleth wa.|ドラゴンはパネルを持っていません|
|Bíi ril bama óowamid wa.|ドラゴンは怒っています|
|Bíi ril di óowamid, “Dóo, ril néde lehóo dihomedaleth íi wa!”|「えーと、私もパネルが欲しい!」とドラゴンは言った|

## 練習問題

例文を参考に空白を埋めて下さい。

【例】 Bíi ril néde le dihomedaleth wa. 私はパネルが欲しいです。

1. Bíi ril néde le _________ wa. 【rimáayo】 1枚のケープ(袖なし肩マント)、ケープ、岬
1. Bíi ril néde le _________ wa. 【don】 1つの櫛/髪飾り/髪留め/(鶏の)とさか、櫛/髪飾り/髪留め/(鶏の)とさか
1. Bíi ril néde le _________ wa. 【lalen】 1本のギター、ギター
1. Bíi ril néde le _________ wa. 【lolin】 1つのロッキングチェア(揺り椅子)、ロッキングチェア(揺り椅子)
