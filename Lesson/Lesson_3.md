# レッスン 3 大会にはスーツケースが必要です

## 3-1. このレッスンで使う単語

|||
|:-:|-|
|imedim|スーツケース|
|thi|～を持っている|
|boó|3 **数詞**|
|be|彼、彼女、それ|
|||
|‐th|直接目的語を表す**接尾辞**|

## 3-2. 例文

|||
|-|-|
|Bíi ril thi Athid imedimeth wa.|アシドはスーツケースを持っています|
|Bíi ril thi Sha imedimeth wa.|シャはスーツケースを持っています|
|Bíi ril thi óowamid imedimeth boó wa.|ドラゴンは3つのスーツケースを持っています|
|Bíi ril thi be imedimeth wa.|彼/彼女/その人はスーツケースを持っています|
|Bíi ril thi be imedimeth boó wa.|彼/彼女/その人は3つのスーツケースを持っています|

## 3-3. 解説

### 直接目的語

||||||||
|-|:-:|:-:|:-:|:-:|:-:|:-:|
|**例文**|Bíi|ril|thi|Adhid|imedim<u>e</u>**th**|wa.|
|**品詞**|発話行為|時制|動詞|主語|目的語+**接尾辞**|証拠|
|**訳**|(宣言する)|(現在)|持っている|アシド|スーツケース**を**|(前提を認識している)|

この文を最も簡素に要約すると「アシドが持っている」となります。持っている物は何かと言えば「スーツケース」です。つまり「アシドが起こした行動」に対し、「その行動の対象」が指定されています。この行動の対象の事を**直接目的語**と言います。

直接目的語は対象となる単語の語尾に **‐th** を付ける事で表します。この接尾辞は子音で始まっているため、対象の語尾が子音で終わっている場合はラーダンのルールに則って **e** を挟む必要があります。

### 数値表現

|||||||||
|-|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|**例文**|Bíi|ril|thi|óowamid|imedimeth|**boó**|wa.|
|**品詞**|発話行為|時制|動詞|主語|目的語+接尾辞|**数詞**|証拠|
|**訳**|(宣言する)|(現在)|持っている|ドラゴン|スーツケースを|**3**|(前提を認識している)|

何かがいくつあるか正確に指定する必要がある場合は**その単語の直後に数詞を置く**事で表現します。

例の直接目的語 imedimeth boó は「3つのスーツケースを」となり、もし主語が óowamid boó なら「3匹のドラゴン」になります。

## 練習問題

例を参考に空白を埋めて下さい。

【例】 Bíi ril thi Athid **imedimeth** wa. アシドは**スーツケースを**持っています。

1. Bíi ril thi Athid __________ wa. 【áabe】 1冊の本、本
1. Bíi ril thi Athid __________ wa. 【ni】 1つのコップ、コップ
1. Bíi ril thi Athid __________ wa. 【dahan】 1台のベッド、ベッド
1. Bíi ril thi Athid __________ wa. 【webe】 1杯のビール、ビール
