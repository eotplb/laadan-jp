# 奥付

## リンク

**公式サイト**

	https://laadanlanguage.com/

**Amberwind's Láadan Lessons**

	http://laadanlanguage.org/

**Láadan @ ayadanconlangs.⁠com**

	http://laadan.ayadanconlangs.com/

## 補足

- 誤った解説は修正しています(特に第1世代から第2世代で修正のあったもの)。

## ライセンス

**クリエイティブ･コモンズ 表示 - 継承 3.0 非移植 (CC BY-SA 3.0)**

	https://creativecommons.org/licenses/by-sa/3.0/deed.ja

このPDFのオリジナルソース(Markdown形式)は**GitLab**で公開しています。

	https://gitlab.com/eotplb/laadan-jp

## 免責事項

- **無保証**です。これらのドキュメントを利用した結果、何らかの被害や損害を被ったとしても制作者は一切責任を取りません。
- **暫定版です。⁠**公式サイトの内容は第1世代と第2世代が混在しているため、第2世代に完全に対応出来ていない可能性があります。
- 万一記述に誤りがあったとしても、**修正する義務を負いません。**
- 将来文法や仕様に変更があったとしても、**追従して修正する義務を負いません。**
