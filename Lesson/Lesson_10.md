# レッスン 10 ドラゴンは売り場を大会に所有しています

## 10-1. このレッスンで使う単語

|||
|-|-|
|áabe|本|
|dínídin|玩具|
|eb|売る、買う|
|losh|お金|
|menedebe|沢山の|
|dalehebewan|売り場|
|lezh|私達(2～5人)|
|||
|id|その後、それから、その上|

## 10-2. 例文

|||
|-|-|
|Bíi ril thi óowamid dalahebewaneth buzheha wa.|ドラゴンは売り場を大会に所有しています|
|Bíi aril eb óowamid áabeth menedebe wa.|ドラゴンは沢山の本を売っているでしょう|
|Bíi aril eb be dínídineth menedebe wa.|その人は沢山の玩具を売っているでしょう|
|Bíi id aril thi óowamid losheth menedebe wa!|その後ドラゴンは沢山のお金を得た事でしょう|

## 10-3. 解説

### (名詞の)修飾詞

|||||||||
|-|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|**例文**|Bíi|aril|eb|óowamid|áabeth|**menedebe**|wa.|
|**品詞**|発話行為|時制|動詞|主語|目的語+接尾辞|**修飾詞**|証拠|
|**訳**|(宣言)|未来|売る|ドラゴン|本を|**沢山の**|(自認)|

名詞を修飾する単語を**修飾詞**と言い、名詞の直後に置きます。

例文では目的語の名詞を修飾しているので、その直後に置いています。

## 練習問題

例文を参考に空白を埋めて下さい。

【例】 Bíi aril meheb lezh **doyuth** menedebe wa.  私達は沢山の**リンゴを**売るでしょう。

1. Bíi aril meheb lezh _________ menedebe wa. 【esh】 ボート
1. Bíi aril meheb lezh _________ menedebe wa. 【lulin】 揺り籠
1. Bíi aril meheb lezh _________ menedebe wa. 【shinehal】 コンピューター
1. Bíi aril meheb lezh _________ menedebe wa. 【shida】 ゲーム

---

【補足】

- **母音が連続してはならない**決まりがあるため、その場合は間に **h** を挟みます。
- 行動の対象が複数の場合、複数形にするのは動詞のみです。目的語(形容詞を含む)を複数形にする必要はありません。
