# 練習問題の解答

## Lesson 2

1. Bíi ril sháad óowamid **bethudedi** wa.
2. Bíi ril sháad óowamid **olinedi** wa.
3. Bíi ril sháad óowamid **miwithedi** wa.
4. Bíi ril sháad óowamid **yodi** wa.

## Lesson 3

1. Bíi ril thi Athid **áabeth** wa.
2. Bíi ril thi Athid **nith** wa.
3. Bíi ril thi Athid **dahaneth** wa.
4. Bíi ril thi Athid **webeth** wa.

## Lesson 4

1. Báa ril sháad ne **bothedi**?
2. Báa ril sháad ne **óoledi**?
3. Báa ril sháad ne **meladi**?
4. Báa ril sháad ne **shodedi**?

## Lesson 5

1. Bíi ril sháad be buzhedi **eshenan** wa.
2. Bíi ril sháad be buzhedi **memazhenan** wa.
3. Bíi ril sháad be buzhedi **zhazhenan** wa.

## Lesson 6

1. Bíi eril di Athid wa, “Báa ril sháad ne **sheshihothedi**?”
2. Bíi eril di Athid wa, “Báa ril sháad ne **wethedi**?”
3. Bíi eril di Athid wa, “Báa ril sháad ne **amedaradi**?”
4. Bíi eril di Athid wa, “Báa ril sháad ne **olinedi**?”

## Lesson 7

1. Bíi ril wida Athid **áabeth** wa.
2. Bíi ril wida Athid **ridademeth** wa.
3. Bíi ril wida Athid **ruleth** wa.
4. Bíi ril wida Athid **doyuth** wa.

---

## Lesson 8

1. Bíi ril merilin bezh **éebeth** wáa.
2. Bíi ril merilin bezh **laleth** wáa.
3. Bíi ril merilin bezh **zhuth** wáa.
4. Bíi ril merilin bezh **ilith** wáa.

## Lesson 9

1. Bíi ril néde le **rimáayoth** wa.
2. Bíi ril néde le **doneth** wa.
3. Bíi ril néde le **laleneth** wa.
4. Bíi ril néde le **lolineth** wa.

## Lesson 10

1. Bíi aril meheb lezh **esheth** menedebe wa.
2. Bíi aril meheb lezh **lulineth** menedebe wa.
3. Bíi aril meheb lezh **shinehaleth** menedebe wa.
4. Bíi aril meheb lezh **shidath** menedebe wa.
