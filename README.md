# 第2世代 Láadan 日本語訳

Láadan(ラーダン)の日本語訳です。

## 内容

大きく分けて以下の3種類の文書があります。

### Láadan 公式レッスン

公式サイトのレッスンを翻訳したものです(第1世代)。

### Láadan 2nd クイックリファレンス

第2世代ラーダンの主な文法を解説したものです。**全ての文法を網羅していません。**

公式レッスンを習得済みである事を前提としています。

### Láadan 2nd 接辞一覧

接辞の一覧表です(第1世代/第2世代 両対応)。

## ライセンス

[クリエイティブ･コモンズ 表示 - 継承 3.0 非移植 (CC BY-SA 3.0)](https://creativecommons.org/licenses/by-sa/3.0/deed.ja)

## 免責事項

- **無保証**です。これらの文書を利用した結果、何らかの被害や損害を被ったとしても制作者は一切責任を取りません。
- **暫定版です。⁠**情報源となったサイトの内容は第1世代と第2世代が混在しているため、第2世代に完全に対応出来ていない可能性があります。
- 万一記述に誤りがあったとしても、**修正する義務を負いません。**
- 将来文法や仕様に変更があったとしても、**追従して修正する義務を負いません。**

## PDF版

[Dropbox](https://www.dropbox.com/sh/gl69bklyhcm86oh/AADWQagPoDXP1P4KlBaYgRaCa?dl=0
)からダウンロードして下さい。
